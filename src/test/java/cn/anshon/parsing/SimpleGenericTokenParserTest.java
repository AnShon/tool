package cn.anshon.parsing;

import org.junit.jupiter.api.Test;

import java.util.AbstractCollection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SimpleGenericTokenParserTest {

  public static class TestBean {
    private String key1;
    private int key2;

    public TestBean() {
    }

    public TestBean(String key1) {
      this.key1 = key1;
    }

    public String getKey1() {
      return key1;
    }

    public void setKey1(String key1) {
      this.key1 = key1;
    }

    public int getKey2() {
      return key2;
    }

    public void setKey2(int key2) {
      this.key2 = key2;
    }
  }

  public static class TestBean1 {
    private String key1;
    private int key2;

    public TestBean1(String key1) {
      this.key1 = key1;
    }

    public String getKey1() {
      return key1;
    }

    public void setKey1(String key1) {
      this.key1 = key1;
    }

    public int getKey2() {
      return key2;
    }

    public void setKey2(int key2) {
      this.key2 = key2;
    }
  }

  public interface i1 {
  }

  public class s1 implements i1 {
  }

  public class s2 extends s1 {
  }

  public class c1 extends s2 {
  }

  @Test
  void superClassTest() throws IllegalAccessException, InstantiationException {
    System.out.println(AbstractCollection.class.getSuperclass());
    Map<Object, Object> cmap = new ConcurrentHashMap<>();
    Map<Object, Object> newMap = cmap.getClass().newInstance();
    System.out.println(newMap.getClass());
  }

  @Test
  void parseSourceTest() throws NoSuchFieldException {

    Map<Object, Object> map = new HashMap<>();
    map.put("1", 12);
    map.put("first_name", "${first_name}");
    map.put(1, 12);
    map.put(null, 12);
    map.put(1223213, (String) null);
    map.put(123, null);
    // 将type强转成Parameterized
    //    TypeVariable<? extends Class<? extends Map>>[] typeVariable =
    //            map.getClass().getTypeParameters();
    //    map.getClass().getGenericSuperclass();
    //    System.out.println(typeVariable.length);
    //    System.out.println(typeVariable[1]);
    SimpleGenericTokenParser parser =
            SimpleGenericTokenParser.builder()
                    .token("${", "}")
                    .addFieldVal(new TestBean("key1val"))
                    .addFieldVal(new TestBean1("key1val"))
                    .addFieldVal(map)
                    .addFieldVal("first_name", "James")
                    .addFieldVal("initial", "T")
                    .addFieldVal("last_name", "Kirk")
                    .addFieldVal("var{with}brace", "Hiya")
                    .addFieldVal("", "")
                    .build();
    Map<Object, Object> nmap = parser.parse(map);
    nmap.forEach(
            (k, v) -> {
              System.out.println(k + ":" + v);
              if (k instanceof Number && (Integer) k == 1223213) {
                System.out.println(v == null);
              }
            });
  }

  @Test
  void parseTest() {
    Map<Object, Object> map = new HashMap<>();
    map.put("1", 12);
    map.put(1, 12);
    map.put(null, 12);
    map.put(123, null);
    SimpleGenericTokenParser parser =
            SimpleGenericTokenParser.builder()
                    .token("${", "}")
                    .addFieldVal(new TestBean("key1val"))
                    .addFieldVal(new TestBean1("key1val"))
                    .addFieldVal(map)
                    .addFieldVal("first_name", "James")
                    .addFieldVal("initial", "T")
                    .addFieldVal("last_name", "Kirk")
                    .addFieldVal("var{with}brace", "Hiya")
                    .addFieldVal("", "")
                    .build();
    TestBean testBean = new TestBean("Hello captain ${first_name} ${initial} ${last_name}");
    TestBean testBeanParsed = parser.parse(testBean);
    assertEquals("Hello captain James T Kirk", testBeanParsed.getKey1());
    assertEquals(
            "key1val reporting.",
            parser.parse("${cn.anshon.parsing.SimpleGenericTokenParserTest$TestBean.key1} reporting."));
    assertEquals(
            "James T Kirk reporting.",
            parser.parse("${first_name} ${initial} ${last_name} reporting."));
    assertEquals(
            "Hello captain James T Kirk",
            parser.parse("Hello captain ${first_name} ${initial} ${last_name}"));
    assertEquals("James T Kirk", parser.parse("${first_name} ${initial} ${last_name}"));
    assertEquals("JamesTKirk", parser.parse("${first_name}${initial}${last_name}"));
    assertEquals("{}JamesTKirk", parser.parse("{}${first_name}${initial}${last_name}"));
    assertEquals("}JamesTKirk", parser.parse("}${first_name}${initial}${last_name}"));

    assertEquals("}James{{T}}Kirk", parser.parse("}${first_name}{{${initial}}}${last_name}"));
    assertEquals("}James}T{Kirk", parser.parse("}${first_name}}${initial}{${last_name}"));
    assertEquals("}James}T{Kirk", parser.parse("}${first_name}}${initial}{${last_name}"));
    assertEquals("}James}T{Kirk{{}}", parser.parse("}${first_name}}${initial}{${last_name}{{}}"));
    assertEquals(
        "}James}T{Kirk{{}}", parser.parse("}${first_name}}${initial}{${last_name}{{}}${}"));

    assertEquals(
        "{$$something}JamesTKirk",
        parser.parse("{$$something}${first_name}${initial}${last_name}"));
    assertEquals("${", parser.parse("${"));
    assertEquals("${\\}", parser.parse("${\\}"));
    assertEquals("Hiya", parser.parse("${var{with\\}brace}"));
    assertEquals("", parser.parse("${}"));
    assertEquals("}", parser.parse("}"));
    assertEquals("Hello ${ this is a test.", parser.parse("Hello ${ this is a test."));
    assertEquals("Hello } this is a test.", parser.parse("Hello } this is a test."));
    assertEquals("Hello } ${ this is a test.", parser.parse("Hello } ${ this is a test."));
  }
}
