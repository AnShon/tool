package cn.anshon.script;

import org.junit.jupiter.api.Test;

import javax.script.ScriptException;
import javax.script.*;
import java.util.List;

class JsScriptExecutorTest {

    @Test
    void compEngine() throws ScriptException, NoSuchMethodException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");

        engine.put("counter", 0); // 向javascript传递一个参数
        Compilable compEngine = (Compilable) engine;
        CompiledScript script1 =
                compEngine.compile(
                        "function count() { "
                                + " counter = counter +1; "
                                + " return counter; "
                                + "}; count();");

        CompiledScript script2 =
                compEngine.compile(
                        "function count1() { "
                                + " counter = counter +1; "
                                + " return counter; "
                                + "}; count1();");
        System.out.println(script1.eval());
        System.out.println(script2.eval());

        Invocable invokeEngine = (Invocable) engine;
        System.out.println(invokeEngine.invokeFunction("count"));
        System.out.println(engine.get("a"));
    }

    @Test
    void engine() throws ScriptException, NoSuchMethodException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");
        engine.put("a", 1);
        String script1 = "function test1() {return 'test1';} function test11() {a=12} test11()";
        System.out.println(engine.eval(script1));
        String script2 = "function test2() {return 'test2';}";
        engine.eval(script2);
        Invocable invokeEngine = (Invocable) engine;
        System.out.println(invokeEngine.invokeFunction("test1"));
        System.out.println(engine.get("a"));
    }

    @Test
    void testGetScriptEngineInfo() {
        ScriptEngineManager manager = new ScriptEngineManager();

        // 得到所有的脚本引擎工厂
        List<ScriptEngineFactory> factories = manager.getEngineFactories();
        // 这是Java SE 5 和Java SE 6的新For语句语法
        for (ScriptEngineFactory factory : factories) {
            // 打印脚本信息
            System.out.printf(
                    "Name: %s%n"
                            + "Version: %s%n"
                            + "Language name: %s%n"
                            + "Language version: %s%n"
                            + "Extensions: %s%n"
                            + "Mime types: %s%n"
                            + "Names: %s%n",
                    factory.getEngineName(),
                    factory.getEngineVersion(),
                    factory.getLanguageName(),
                    factory.getLanguageVersion(),
                    factory.getExtensions(),
                    factory.getMimeTypes(),
                    factory.getNames());
            // 得到当前的脚本引擎
            ScriptEngine engine = factory.getScriptEngine();
        }
    }

    @Test
    void testGet() {
        System.out.println(JsScriptExecutor.builder().put("test", "a").build().get("test").toString());
    }

    @Test
    void testEval() {
    }
}
