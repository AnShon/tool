package cn.anshon.map;

import java.util.Map;

/**
 * Description: MapUtils
 *
 * @author anshun
 * @version 2020/7/31 16:21
 */
public class MapUtils {
    /**
     * map空判断
     *
     * @param map map
     * @param <K> key的类型
     * @param <V> value的类型
     * @return 是否为空
     */
    public static <K, V> boolean isEmpty(Map<K, V> map) {
        return map == null || map.isEmpty();
    }

    /**
     * map空判断
     *
     * @param map map
     * @param <K> key的类型
     * @param <V> value的类型
     * @return 是否为空
     */
    public static <K, V> boolean isNotEmpty(Map<K, V> map) {
        return !isEmpty(map);
    }
}
