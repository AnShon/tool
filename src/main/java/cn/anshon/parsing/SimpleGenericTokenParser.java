package cn.anshon.parsing;

import cn.anshon.array.ArrayUtils;
import cn.anshon.string.StringUtils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Description: 占位符解析 默认占位符 ${}
 *
 * @author anshun
 * @version 2020/7/29 9:12
 */
public class SimpleGenericTokenParser extends GenericTokenParser {
  private static final char NESTED = '.';

  public SimpleGenericTokenParser(String openToken, String closeToken, TokenHandler handler) {
    super(openToken, closeToken, handler);
  }

  public static Builder builder() {
    return new Builder();
  }

  /**
   * 将属性值的占位符进行替换
   *
   * @param source 资源
   * @param <K>    key数据类型
   * @param <V>    value数据类型
   * @return 返回一个资源的副本
   */
  @SuppressWarnings("unchecked")
  public <K, V> Map<K, V> parse(Map<K, V> source) {
    if (null == source) {
      return null;
    }
    try {
      Map<K, V> target = source.getClass().newInstance();
      source.forEach(
              (k, v) -> {
                if (v instanceof String) {
                  target.put(k, (V) parse((String) v));
                } else {
                  target.put(k, v);
                }
          });
      return target;
    } catch (Exception e) {
      throw new ParsingException(e);
    }
  }

  /**
   * 将属性值的占位符进行替换
   *
   * @param source 资源
   * @param <T> 数据类型
   * @return 返回一个资源的副本
   */
  @SuppressWarnings("unchecked")
  public <T> T parse(T source) {
    if (null == source) {
      return null;
    }
    try {
      T target = (T) (source.getClass().newInstance());
      Set<String> ignoreFieldSet = ignoreFields(target, ParseIgnoreField.class);
      BeanInfo beanInfo = Introspector.getBeanInfo(source.getClass());
      PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
      for (PropertyDescriptor property : propertyDescriptors) {
        String key = property.getName();
        if (key.compareToIgnoreCase("class") == 0 || ignoreFieldSet.contains(key)) {
          continue;
        }
        Method setter = property.getWriteMethod();
        if (setter == null) {
          continue;
        }
        Class<?>[] paramTypes = setter.getParameterTypes();
        if (paramTypes.length == 1 && paramTypes[0] == String.class) {
          // 获取原始值
          Method getter = property.getReadMethod();

          Object value = getter != null ? getter.invoke(source) : null;
          if (value != null) {
            setter.invoke(target, parse(value.toString()));
          }
        }
      }
      return target;
    } catch (Exception e) {
      throw new ParsingException(e);
    }
  }
  /** 默认占位符 ${} */
  public static class Builder {
    private String openToken = "${";
    private String closeToken = "}";
    private final Map<String, String> variables = new HashMap<>();
    private final Set<Class<?>> classSet = new HashSet<>();

    public SimpleGenericTokenParser build() {
      return new SimpleGenericTokenParser(
              openToken, closeToken, new SimpleTokenHandler(variables, classSet));
    }

    public Builder token(final String openToken, final String closeToken) {
      this.openToken = openToken;
      this.closeToken = closeToken;
      return this;
    }

    /**
     * 添加k,v
     *
     * @param source 资源类
     * @param ignoreFields 忽略属性名称
     * @return Builder
     */
    public Builder addFieldVal(final Object source, String... ignoreFields) {
      if (null != source) {
        Set<String> ignoreFieldSet = ignoreFields(source, AddIgnoreField.class);
        if (ArrayUtils.isNotEmpty(ignoreFields)) {
          ignoreFieldSet.addAll(Arrays.asList(ignoreFields));
        }
        if (source instanceof Map) {
          for (Map.Entry<?, ?> entry : ((Map<?, ?>) source).entrySet()) {
            Object k = entry.getKey();
            Object v = entry.getValue();
            String key;
            if (k == null) {
              variables.put(null, String.valueOf(v));
            } else if (!ignoreFieldSet.contains((key = String.valueOf(k)))) {
              variables.put(key, String.valueOf(v));
            }
          }
        } else {
          try {
            BeanInfo beanInfo = Introspector.getBeanInfo(source.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            Class<?> clazz = source.getClass();
            classSet.add(clazz);
            String prefix = clazz.getName();
            for (PropertyDescriptor property : propertyDescriptors) {
              String key = property.getName();
              if (key.compareToIgnoreCase("class") == 0 || ArrayUtils.contains(ignoreFields, key)) {
                continue;
              }
              // 获取原始值
              Method getter = property.getReadMethod();
              Object value = getter != null ? getter.invoke(source) : null;
              variables.put(prefix + NESTED + key, String.valueOf(value));
            }
          } catch (Exception e) {
            throw new ParsingException(e);
          }
        }
      }
      return this;
    }

    public Builder addFieldVal(final String key, final String v) {
      variables.put(key, v);
      return this;
    }
  }

  private static <T extends Annotation> Set<String> ignoreFields(
          Object source, Class<T> annotation) {
    Set<String> ignoreFields = new HashSet<>();
    if (null != source) {
      try {
        Field[] fields = source.getClass().getDeclaredFields();
        for (Field field : fields) {
          if (field.getAnnotation(annotation) != null) {
            ignoreFields.add(field.getName());
          }
        }
      } catch (Exception e) {
        throw new ParsingException(e);
      }
    }
    return ignoreFields;
  }

  /**
   * Description:
   *
   * @author anshun
   * @version 2020/7/29 9:18
   */
  private static class SimpleTokenHandler implements TokenHandler {
    private final Map<String, String> variables;
    private final Set<Class<?>> classSet;

    private SimpleTokenHandler(Map<String, String> variables, Set<Class<?>> classSet) {
      this.variables = variables;
      this.classSet = classSet;
    }

    @Override
    public String handleToken(String content) {
      if (variables.containsKey(content)) {
        return variables.get(content);
      } else {
        List<String> keyList = new ArrayList<>();
        String key = "";
        for (Class<?> clazz : classSet) {
          key = clazz.getName() + NESTED + content;
          if (variables.containsKey(key)) {
            keyList.add(key);
          }
        }
        if (keyList.size() == 0) {
          return null;
        } else if (keyList.size() == 1) {
          return variables.get(keyList.get(0));
        } else {
          throw new ParsingException(
                  " key '"
                          + content
                          + "' in field list is ambiguous! keys->["
                          + StringUtils.join(keyList, ",")
                          + "]");
        }
      }
    }
  }
}
