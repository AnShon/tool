package cn.anshon.parsing;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;

/**
 * Description: 添加source需要忽略的字段
 *
 * @author anshun
 * @version 2020/7/31 10:08
 * @since 1.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {FIELD})
public @interface AddIgnoreField {
}
