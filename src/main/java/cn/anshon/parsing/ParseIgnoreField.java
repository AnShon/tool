package cn.anshon.parsing;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;

/**
 * 替换示例属性时需要忽略的属性
 *
 * @author anshun
 * @version 2020/7/31 10:08
 * @since 1.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {FIELD})
public @interface ParseIgnoreField {
}
