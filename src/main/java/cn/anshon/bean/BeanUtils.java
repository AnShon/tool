package cn.anshon.bean;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

/**
 * Description: bean 操作类
 *
 * @author anshun
 * @version 2020/8/3 11:33
 */
public class BeanUtils {

    /**
     * clone bean 属性提供了getter和setterc才能拷贝
     *
     * @param source 资源
     * @return 拷贝的副本
     */
    public static Object clone(Object source)
            throws IllegalAccessException, InstantiationException, IntrospectionException {
        if (source == null) {
            return null;
        }
        Object target = source.getClass().newInstance();
        BeanInfo beanInfo = Introspector.getBeanInfo(source.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();

        return target;
    }
}
