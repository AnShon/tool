package cn.anshon.collection;

import java.util.Collection;

/**
 * Description: Collection工具类
 *
 * @author anshun
 * @version 2020/8/3 17:12
 */
public class CollectionUtils {

    /**
     * 容器是否为空
     *
     * @param collection 容器
     * @return 是否为空
     */
    public static <T> boolean isEmpty(Collection<T> collection) {
        return collection == null || collection.size() == 0;
    }

    /**
     * 容器是否为非空
     *
     * @param collection 容器
     * @return 是否为非空
     */
    public static <T> boolean isNotEmpty(Collection<T> collection) {
        return !isEmpty(collection);
    }
}
