package cn.anshon.script;

import cn.anshon.map.MapUtils;
import cn.anshon.string.StringUtils;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleBindings;
import java.util.HashMap;
import java.util.Map;

/**
 * Description: Js 执行器
 *
 * @author anshun
 * @version 2020/7/31 9:37
 */
public class JsScriptExecutor implements ScriptExecutor {
    /**
     * 脚本执行引擎
     */
    private ScriptEngine engine;

    private JsScriptExecutor(ScriptEngine engine) {
        this.engine = engine;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Retrieves a value set in the state of this engine. The value might be one which was set using
     * <code>setValue</code> or some other value in the state of the <code>ScriptEngine</code>,
     * depending on the implementation. Must have the same effect as <code>
     * getBindings(ScriptContext.ENGINE_SCOPE).get</code>
     *
     * @param key The key whose value is to be returned
     * @return the value for the given key
     * @throws NullPointerException     if key is null.
     * @throws IllegalArgumentException if key is empty.
     */
    public <T> T get(String key) {
        return (T) engine.get(key);
    }

    /**
     * Executes the specified script. The default <code>ScriptContext</code> for the <code>
     * ScriptEngine</code> is used.
     *
     * <p>脚本中调用java
     *
     * <p><code>var MyJavaClass = Java.type('com.goodsogood.ows.utils.QuestionnaireHelper');</code>,
     *
     * <p><code>importPackage() 和 importClass() 函数来导入 Java 的包和类</code>, 可以通过 Packages 全局变量来访问 Java
     * 包，例如Packages.java.util.Vector 或者 Packages.javax.swing.JFrame。但标准的 Java SE 包有更简单的访问方式，如： java 对应
     * Packages.java, javax 对应 Packages.javax, 以及 org 对应 Packages.org。
     *
     * <p>java.lang 包默认不需要导入，因为这会和 Object、Boolean、Math 等其他 JavaScript 内建的对象在命名上冲突。此外，导入任何 Java
     * 包和类也可能导致 JavaScript 全局作用域下的变量名冲突。为了避免冲突，我们定义了一个 JavaImporter 对象， 并通过 with 语句来限制导入的 Java 包和类的作用域
     * <code>
     * // Create a JavaImporter object with specified packages and classes to import
     * var
     * Gui = new
     * JavaImporter(java.awt, javax.swing);
     * // Pass the JavaImporter object to the "with" statement and access the classes
     * // from the imported packages by their simple names within the statement's body
     * with
     * (Gui) {
     * var
     * awtframe = new
     * Frame("AWT Frame");
     * var
     * jframe = new
     * JFrame("Swing JFrame");
     * };
     * </code>
     *
     * <p><code>var MyJavaClass = Java.type('com.goodsogood.ows.utils.QuestionnaireHelper');</code>,
     *
     * @param script The script language source to be executed.
     * @return The value returned from the execution of the script.
     * @throws javax.script.ScriptException if error occurs in script.
     * @throws NullPointerException         if the argument is null.
     */
    public <T> T eval(String script) {
        try {
            return (T) engine.eval(script);
        } catch (javax.script.ScriptException e) {
            throw new ScriptException(e);
        }
    }

    public static class Builder {
        /**
         * 向引擎中传递的变量
         */
        private Map<String, Object> variables = new HashMap<>();

        public JsScriptExecutor build() {

            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("JavaScript");
            if (MapUtils.isNotEmpty(variables)) {
                engine.setBindings(new SimpleBindings(variables), ScriptContext.ENGINE_SCOPE);
            }
            return new JsScriptExecutor(engine);
        }

        /**
         * 给js传递变量
         *
         * @param key   变量名称
         * @param value 值
         * @return Builder
         */
        public Builder put(String key, Object value) {
            if (StringUtils.isBlank(key)) {
                throw new ScriptException("Key can not be blank !");
            }
            variables.put(key, value);
            return this;
        }

        /**
         * 给js传递变量
         *
         * @param map 变量 k-v
         * @return Builder
         */
        public Builder put(Map<String, Object> map) {
            if (null != map && !map.isEmpty()) {
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (StringUtils.isBlank(key)) {
                        throw new ScriptException("Key can not be blank !");
                    }
                    variables.put(key, entry.getValue());
                }
            }
            return this;
        }
    }
}
